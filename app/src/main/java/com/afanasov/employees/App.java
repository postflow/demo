package com.afanasov.employees;

import android.app.Application;


import com.afanasov.employees.di.components.AppComponent;
import com.afanasov.employees.di.components.DaggerAppComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class App extends Application {


  private static AppComponent appComponent;

  public static AppComponent getAppComponent() {
    return appComponent;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    Realm.init(this);
    RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
      .deleteRealmIfMigrationNeeded()
      .build();
    Realm.setDefaultConfiguration(realmConfiguration);

    appComponent = DaggerAppComponent.builder().build();
  }




}
