package com.afanasov.employees.data.retrofit;


import com.afanasov.employees.data.retrofit.retrofitmodel.PerformerRetrofitModel;
import java.util.List;
import io.reactivex.Single;

public interface IRepositoryNetwork {
  Single<List<PerformerRetrofitModel>> loadPerformersFromRest( );
}
