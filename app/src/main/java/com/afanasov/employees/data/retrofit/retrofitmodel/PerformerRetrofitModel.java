
package com.afanasov.employees.data.retrofit.retrofitmodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PerformerRetrofitModel {

    @SerializedName("f_name")
    @Expose
    private String fName;
    @SerializedName("l_name")
    @Expose
    private String lName;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("avatr_url")
    @Expose
    private String avatrUrl;
    @SerializedName("specialty")
    @Expose
    private List<SpecialtyRetrofitModel> specialty = null;

    public String getFName() {
//      String fName = response.getFName();
//      String lName = response.getLName();
//      fName = fName.substring(0,1).toUpperCase() + fName.substring(1).toLowerCase();
//      lName = lName.substring(0,1).toUpperCase() + lName.substring(1).toLowerCase();

        return fName.substring(0,1).toUpperCase() + fName.substring(1).toLowerCase();
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getLName() {
        return lName.substring(0,1).toUpperCase() + lName.substring(1).toLowerCase();
    }

    public void setLName(String lName) {
        this.lName = lName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatrUrl() {
        return avatrUrl;
    }

    public void setAvatrUrl(String avatrUrl) {
        this.avatrUrl = avatrUrl;
    }

    public List<SpecialtyRetrofitModel> getSpecialty() {
        return specialty;
    }

    public void setSpecialty(List<SpecialtyRetrofitModel> specialty) {
        this.specialty = specialty;
    }


  public int getPerformerId(){
    int result = fName != null ? fName.hashCode() : 0;
    result = 31 * result + (lName != null ? lName.hashCode() : 0);
    result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
    result = 31 * result + (avatrUrl != null ? avatrUrl.hashCode() : 0);
    return result;
    }
}
