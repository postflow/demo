package com.afanasov.employees.data.retrofit;

import com.afanasov.employees.data.retrofit.retrofitmodel.PerformerListRetrofitModel;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface IRetrofitService {
    @GET("testapi.json")
    Single<PerformerListRetrofitModel> getPerformersSingle();
}
