package com.afanasov.employees.data.retrofit;


import com.afanasov.employees.Config;
import com.afanasov.employees.data.retrofit.retrofitmodel.PerformerListRetrofitModel;
import com.afanasov.employees.data.retrofit.retrofitmodel.PerformerRetrofitModel;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepositoryNetwork implements IRepositoryNetwork {

  private IRetrofitService retrofitService;

  public RepositoryNetwork() {
    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
    logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    httpClient.addInterceptor(logging);

    String baseUrl = Config.BASE_URL;
    Retrofit client = new Retrofit.Builder()
      .baseUrl(baseUrl)
      .client(httpClient.build())
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .build();
    retrofitService = client.create(IRetrofitService.class);
  }

  @Override
  public Single<List<PerformerRetrofitModel>> loadPerformersFromRest() {
    return retrofitService.getPerformersSingle()
      .map(PerformerListRetrofitModel::getPerformerRetrofitModel);
  }
}
