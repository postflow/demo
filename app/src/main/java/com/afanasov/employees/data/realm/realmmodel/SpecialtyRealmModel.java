package com.afanasov.employees.data.realm.realmmodel;


import io.realm.RealmObject;


public class SpecialtyRealmModel extends RealmObject {


  private int specialtyId;
  private String specialtyName;

  public SpecialtyRealmModel() {
  }

//  public SpecialtyRealmModel(int specialtyId, String specialtyName) {
//    this.specialtyId = specialtyId;
//    this.specialtyName = specialtyName;
//  }

  public static SpecialtyRealmModel create(int specialtyId, String specialtyName) {
    SpecialtyRealmModel specialtyRealmModel = new SpecialtyRealmModel();
    specialtyRealmModel.specialtyId = specialtyId;
    specialtyRealmModel.specialtyName = specialtyName;
    return specialtyRealmModel;
  }


  public int getSpecialtyId() {
    return specialtyId;
  }

  public String getSpecialtyName() {
    return specialtyName;
  }

}
