package com.afanasov.employees.data.repository;

import com.afanasov.employees.App;
import com.afanasov.employees.data.realm.IRepositoryDB;
import com.afanasov.employees.data.realm.realmmodel.PerformerRealmModel;
import com.afanasov.employees.data.retrofit.IRepositoryNetwork;
import com.afanasov.employees.domain.LocalStorageException;
import com.afanasov.employees.domain.MapperManager;
import com.afanasov.employees.domain.NetworkConditionsException;
import com.afanasov.employees.domain.SpecialtyQualifier;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class Repository implements IRepository {

  @Inject
  IRepositoryNetwork mRepositoryNetwork;

  @Inject
  IRepositoryDB mRepositoryDB;

  public Repository() {
    App.getAppComponent().inject(this);
  }

  @Override
  public Flowable<List<PerformerViewModel>> loadAllPerformers() {
    return Single.merge(loadFromDB(), loadFromNetworkWithSaveToDB())
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread());
  }

  @Override
  public Single<List<PerformerViewModel>> loadPerformerByQualifier(SpecialtyQualifier specialtyQualifier) {
    return Single.fromCallable(() -> mRepositoryDB.readAllPerformerByQualifierFromDB(specialtyQualifier))
      .flatMapObservable(Observable::fromIterable)
      .map(MapperManager::realmModelToViewModel)
      .toList()
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread());
  }

  @Override
  public Single<PerformerViewModel> loadPerformerInfo(int performerId) {
    return Single.fromCallable(() -> mRepositoryDB.readPerformerInfoFromDB(performerId))
      .map(MapperManager::realmModelToViewModel)
      .subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread());
  }

  private Single<List<PerformerViewModel>> loadFromDB() {
    return Single.fromCallable(() -> mRepositoryDB.readAllPerformerByQualifierFromDB())
      .flatMapObservable(Observable::fromIterable)
      .map(MapperManager::realmModelToViewModel)
      .toList()
      .onErrorResumeNext(
        throwable -> Single.error(new LocalStorageException("load from DB is missing" +
          "method: loadFromDB() return Error")));
  }

  private Single<List<PerformerViewModel>> loadFromNetworkWithSaveToDB() {
    return mRepositoryNetwork.loadPerformersFromRest()
      .flatMapObservable(Observable::fromIterable)
      .map(MapperManager::retrofitToViewModel)
      .toList()
      .doOnSuccess(performerViewModelsList -> {
        List<PerformerRealmModel> performerRealmModels = MapperManager.viewModelToRealmModel(performerViewModelsList);
        mRepositoryDB.writeAllToDB(performerRealmModels);
      })
      .onErrorResumeNext(
        throwable -> Single.error(new NetworkConditionsException("Network response is missing" +
          "method: loadFromNetworkWithSaveToDB()return Error")));
  }

}
