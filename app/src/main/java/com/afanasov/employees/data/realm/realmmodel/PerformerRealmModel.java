package com.afanasov.employees.data.realm.realmmodel;



import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PerformerRealmModel extends RealmObject {

  @PrimaryKey
  private int performerId;
  private String firstName;
  private String lastName;
  private String birthday;
  private String avatarUrl;
  private String age;
  private RealmList<SpecialtyRealmModel> specialty = new RealmList<>();

  public PerformerRealmModel() {}

  public static PerformerRealmModel create(int performerId, String firstName, String lastName, String birthday, String age, String avatarUrl, RealmList<SpecialtyRealmModel> specialtyList){
    PerformerRealmModel performerRealmModel = new PerformerRealmModel();
    performerRealmModel.performerId = performerId;
    performerRealmModel.firstName = firstName;
    performerRealmModel.lastName = lastName;
    performerRealmModel.birthday = birthday;
    performerRealmModel.avatarUrl = avatarUrl;
    performerRealmModel.age = age;
    performerRealmModel.specialty = specialtyList;
    return performerRealmModel;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getBirthday() {
    return birthday;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public int getPerformerId() {
    return performerId;
  }

  public String getAge() {
    return age;
  }

  public RealmList<SpecialtyRealmModel> getSpecialty() {
    return specialty;
  }







}
