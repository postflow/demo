
package com.afanasov.employees.data.retrofit.retrofitmodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PerformerListRetrofitModel {

    @SerializedName("response")
    @Expose
    private List<PerformerRetrofitModel> performerRetrofitModel = null;

    public List<PerformerRetrofitModel> getPerformerRetrofitModel() {
        return performerRetrofitModel;
    }

    public void setPerformerRetrofitModel(List<PerformerRetrofitModel> performerRetrofitModel) {
        this.performerRetrofitModel = performerRetrofitModel;
    }



}
