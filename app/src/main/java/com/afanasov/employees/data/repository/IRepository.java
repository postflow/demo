package com.afanasov.employees.data.repository;

import com.afanasov.employees.domain.SpecialtyQualifier;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import java.util.List;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface IRepository {

  Flowable<List<PerformerViewModel>> loadAllPerformers();

  Single<List<PerformerViewModel>> loadPerformerByQualifier(SpecialtyQualifier specialtyQualifier);

  Single<PerformerViewModel> loadPerformerInfo(int performerId);
}
