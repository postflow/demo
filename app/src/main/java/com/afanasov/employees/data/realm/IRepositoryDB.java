package com.afanasov.employees.data.realm;


import com.afanasov.employees.data.realm.realmmodel.PerformerRealmModel;
import com.afanasov.employees.domain.SpecialtyQualifier;
import java.util.List;

public interface IRepositoryDB {

  void writeAllToDB(List<PerformerRealmModel> performerList);

  List<PerformerRealmModel> readAllPerformerByQualifierFromDB();

  PerformerRealmModel readPerformerInfoFromDB(int performerId);

  List<PerformerRealmModel> readAllPerformerByQualifierFromDB(SpecialtyQualifier specialtyQualifier);

}
