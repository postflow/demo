package com.afanasov.employees.data.realm;


import com.afanasov.employees.data.realm.realmmodel.PerformerRealmModel;
import com.afanasov.employees.domain.SpecialtyQualifier;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmResults;


public class RepositoryRealmDB implements IRepositoryDB {


  @Override
  public void writeAllToDB(List<PerformerRealmModel> performerList) {
    Realm realm = Realm.getDefaultInstance();
    realm.executeTransaction(realm1 -> realm1.copyToRealmOrUpdate(performerList));
    realm.close();
  }

  @Override
  public List<PerformerRealmModel> readAllPerformerByQualifierFromDB() {
    Realm realm = Realm.getDefaultInstance();
    RealmResults<PerformerRealmModel> performerRealmModelsList = realm.where(PerformerRealmModel.class).findAll();
    List<PerformerRealmModel> performerRealmModels = realm.copyFromRealm(performerRealmModelsList);
    realm.close();
    return performerRealmModels;

  }

  @Override
  public PerformerRealmModel readPerformerInfoFromDB(int performerId) {
    Realm realm = Realm.getDefaultInstance();
    PerformerRealmModel performerIdresult = realm.where(PerformerRealmModel.class).equalTo("performerId", performerId ).findFirst();
    PerformerRealmModel performerRealmModel = realm.copyFromRealm(performerIdresult);
    realm.close();
    return performerRealmModel;
  }

  @Override
  public List<PerformerRealmModel> readAllPerformerByQualifierFromDB(SpecialtyQualifier specialtyQualifier) {
    Realm realm = Realm.getDefaultInstance();
    RealmResults<PerformerRealmModel> performerRealmModelsByQualifier = realm.where(PerformerRealmModel.class).equalTo("specialty.specialtyId", specialtyQualifier.getSpecialtyId()).findAll();
    List<PerformerRealmModel> performerRealmModels = realm.copyFromRealm(performerRealmModelsByQualifier);
    realm.close();
    return performerRealmModels;
  }
}
