package com.afanasov.employees.domain;


public class LocalStorageException extends Exception {

    public LocalStorageException(String message) {
        super(message);
    }
}