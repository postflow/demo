package com.afanasov.employees.domain;

import com.afanasov.employees.data.realm.realmmodel.PerformerRealmModel;
import com.afanasov.employees.data.realm.realmmodel.SpecialtyRealmModel;
import com.afanasov.employees.data.retrofit.retrofitmodel.PerformerRetrofitModel;
import com.afanasov.employees.data.retrofit.retrofitmodel.SpecialtyRetrofitModel;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.afanasov.employees.presentation.model_view.SpecialtyViewModel;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;

public class MapperManager {

  public static PerformerViewModel retrofitToViewModel(PerformerRetrofitModel performerRetrofitModel){
    List<SpecialtyViewModel> specialtyViewModels = new ArrayList<>();

    for (SpecialtyRetrofitModel specialty : performerRetrofitModel.getSpecialty()) {
      specialtyViewModels.add(new SpecialtyViewModel(specialty.getSpecialtyId(), specialty.getName()));
    }

    String age = calculateAge(performerRetrofitModel.getBirthday());

    return new PerformerViewModel(performerRetrofitModel.getPerformerId(), performerRetrofitModel.getFName(),
      performerRetrofitModel.getLName(),
      performerRetrofitModel.getBirthday(), age,
      performerRetrofitModel.getAvatrUrl(), specialtyViewModels);
  }

  public static List<PerformerRealmModel> viewModelToRealmModel(List<PerformerViewModel> performerViewModelsList){
    List<PerformerRealmModel> performerRealmModelList = new ArrayList<>();

    for ( PerformerViewModel performerViewModel : performerViewModelsList) {
      RealmList<SpecialtyRealmModel> specialtyRealmModelRealmList = viewModelToRealmModelSpecialty(performerViewModel.getSpecialty());

      performerRealmModelList.add( PerformerRealmModel.create(performerViewModel.getPerformerId(),performerViewModel.getFirstName(),
        performerViewModel.getLastName(),performerViewModel.getBirthday(),
        performerViewModel.getAge(),performerViewModel.getAvatarUrl(),specialtyRealmModelRealmList));
    }

    return performerRealmModelList;
  }

  private static RealmList<SpecialtyRealmModel> viewModelToRealmModelSpecialty(List<SpecialtyViewModel> specialtyViewModelList){
    RealmList<SpecialtyRealmModel> specialtyRealmModelRealmList = new RealmList<>();
    for (SpecialtyViewModel specialtyViewModel : specialtyViewModelList){
      specialtyRealmModelRealmList.add(
          SpecialtyRealmModel.create(specialtyViewModel.getSpecialtyId(),specialtyViewModel.getSpecialtyName()));
    }
    return specialtyRealmModelRealmList;
  }

  public static PerformerViewModel realmModelToViewModel(PerformerRealmModel performerRealmModel){
    List<SpecialtyViewModel> specialtyViewModels = new ArrayList<>();

    for (SpecialtyRealmModel specialtyRealmModel : performerRealmModel.getSpecialty()) {
      specialtyViewModels.add( new SpecialtyViewModel(specialtyRealmModel.getSpecialtyId(),specialtyRealmModel.getSpecialtyName()));
    }

    return  new PerformerViewModel(performerRealmModel.getPerformerId(),performerRealmModel.getFirstName(),performerRealmModel.getLastName(),
      performerRealmModel.getBirthday(),performerRealmModel.getAge(),
      performerRealmModel.getAvatarUrl(),specialtyViewModels);
  }

  private static String calculateAge(String birthday){
    if( birthday != null && !birthday.isEmpty()){
      SimpleDateFormat simpleDateFormat = null;
      if(birthday.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")){
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
      } else if (birthday.matches("([0-9]{2})-([0-9]{2})-([0-9]{4})"))
      {
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
      }
      Date date = null;
      try {
        date = simpleDateFormat.parse(birthday);
      } catch (ParseException e) {
        e.printStackTrace();
      }

      LocalDate birthdate = new LocalDate(date);
      LocalDate now = new LocalDate(); // test, in real world without args

      return String.valueOf(Years.yearsBetween(birthdate, now).getYears());
    } else{
      return " - ";
    }

  }

}
