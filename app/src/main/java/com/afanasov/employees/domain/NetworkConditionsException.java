package com.afanasov.employees.domain;


public class NetworkConditionsException extends Exception {
    public NetworkConditionsException(String message) {
        super(message);
    }
}

