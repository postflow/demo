package com.afanasov.employees.domain;


public enum SpecialtyQualifier {

  ALL(100),
  MANAGER(101),
  DEVELOPER(102);

  private int specialtyId;

  SpecialtyQualifier(int specialtyId) {
    this.specialtyId = specialtyId;
  }

  public int getSpecialtyId() {
    return specialtyId;
  }

}
