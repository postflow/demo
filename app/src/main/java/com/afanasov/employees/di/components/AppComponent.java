package com.afanasov.employees.di.components;


import com.afanasov.employees.data.repository.Repository;
import com.afanasov.employees.di.modules.DaggerUtilModule;
import com.afanasov.employees.presentation.presenters.InfoActivityPresenter;
import com.afanasov.employees.presentation.presenters.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {DaggerUtilModule.class})
@Singleton
public interface AppComponent {
  void inject(InfoActivityPresenter infoActivityPresenter);

  void inject(MainActivityPresenter mainActivityPresenter);

  void inject(Repository repository);


}
