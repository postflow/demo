package com.afanasov.employees.di.modules;


import com.afanasov.employees.data.realm.IRepositoryDB;
import com.afanasov.employees.data.realm.RepositoryRealmDB;
import com.afanasov.employees.data.repository.IRepository;
import com.afanasov.employees.data.repository.Repository;
import com.afanasov.employees.data.retrofit.IRepositoryNetwork;
import com.afanasov.employees.data.retrofit.RepositoryNetwork;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DaggerUtilModule {

  @Provides
  @Singleton
  public IRepository provideRepository() {
    return new Repository();
  }
  @Provides
  @Singleton
  public IRepositoryNetwork provideRepositoryNetwork() {
    return new RepositoryNetwork();
  }

  @Provides
  @Singleton
  public IRepositoryDB provideRepositoryDB() {
    return new RepositoryRealmDB();
  }

}
