package com.afanasov.employees.presentation.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afanasov.employees.R;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.squareup.picasso.Picasso;


import java.util.List;

class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

  private List<PerformerViewModel> performerArrayList;
  private RecyclerAdapter.ItemClickListener itemClickListener;

  RecyclerAdapter(List<PerformerViewModel> dataList, RecyclerAdapter.ItemClickListener itemClickListener) {
    this.performerArrayList = dataList;
    this.itemClickListener = itemClickListener;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RecyclerAdapter.ViewHolder viewHolder;
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.cardview, parent, false);
    viewHolder = new RecyclerAdapter.ViewHolder(view, itemClickListener);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    PerformerViewModel itemPerformer = performerArrayList.get(position);
    holder.bindView(itemPerformer);
  }

  @Override
  public int getItemCount() {
    return performerArrayList.size();
  }

  void updateAdapterList(List<PerformerViewModel> performers) {
    performerArrayList.clear();
    performerArrayList.addAll(performers);
    notifyDataSetChanged();
  }

  interface ItemClickListener {
    void onItemClick(PerformerViewModel performer);
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    private View view;
    private TextView firstName;
    private TextView lastName;
    private TextView age;
    private ImageView avatar;
    private PerformerViewModel performer;

    ViewHolder(View v, final ItemClickListener itemClickListener) {
      super(v);
      this.view = v;
      view.setOnClickListener(view -> itemClickListener.onItemClick(performer));
      this.avatar = v.findViewById(R.id.avatar);
      this.firstName = v.findViewById(R.id.first_name);
      this.lastName = v.findViewById(R.id.last_name);
      this.age = v.findViewById(R.id.age);
    }

    void bindView(PerformerViewModel performer) {
      this.performer = performer;
      this.firstName.setText(performer.getFirstName());
      this.lastName.setText(performer.getLastName());
      this.age.setText(String.format("%s %s", view.getContext().getString(R.string.age), performer.getAge()));

      if (performer.getAvatarUrl() == null || performer.getAvatarUrl().isEmpty()) {
        avatar.setImageResource(R.mipmap.ph);
      } else {
        Picasso.with(view.getContext())
          .load(performer.getAvatarUrl())
          .placeholder(R.mipmap.ph)
          .fit()
          .centerCrop()
          .error(R.mipmap.ph)
          .into(avatar);
      }
    }
  }
}
