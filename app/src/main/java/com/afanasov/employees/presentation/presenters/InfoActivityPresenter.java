package com.afanasov.employees.presentation.presenters;

import com.afanasov.employees.App;
import com.afanasov.employees.data.repository.IRepository;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.afanasov.employees.presentation.view.IInfoActivityView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

@InjectViewState
public class InfoActivityPresenter extends MvpPresenter<IInfoActivityView> {

  @Inject
  IRepository mRepository;
  private CompositeDisposable disposables = new CompositeDisposable();

  public InfoActivityPresenter() {
    App.getAppComponent().inject(this);
  }

  @Override
  public void onFirstViewAttach() {
    super.onFirstViewAttach();
  }

  public void loadPerformer(int performerId) {
    disposables.add(mRepository.loadPerformerInfo(performerId)
      .subscribe(
        performerViewModel -> getViewState().updatePerformer(performerViewModel),
        throwable -> getViewState().showLoadError("Loading error. Try again later...")));
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if(!disposables.isDisposed()){
      disposables.dispose();
    }
  }

}
