package com.afanasov.employees.presentation.model_view;



public class SpecialtyViewModel {

  private int specialtyId;
  private String specialtyName;


  public SpecialtyViewModel(int specialtyId, String specialtyName) {
    this.specialtyId = specialtyId;
    this.specialtyName = specialtyName;
  }

  public int getSpecialtyId() {
    return specialtyId;
  }

  public String getSpecialtyName() {
    return specialtyName;
  }

}
