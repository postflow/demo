package com.afanasov.employees.presentation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.afanasov.employees.R;
import com.afanasov.employees.domain.SpecialtyQualifier;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.afanasov.employees.presentation.presenters.MainActivityPresenter;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

public class MainActivity extends MvpAppCompatActivity implements IMainActivityView {

  @InjectPresenter
  MainActivityPresenter mMainActivityPresenter;
  private RecyclerView mRecyclerView;
  private RecyclerAdapter mRecyclerAdapter;
  private SwipeRefreshLayout mSwipeRefreshLayout;
  private Spinner spinner;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
    mSwipeRefreshLayout.setOnRefreshListener(() -> {
      int selectedItemPosition = spinner.getSelectedItemPosition();
      SpecialtyQualifier specialtyQualifier = SpecialtyQualifier.values()[selectedItemPosition];
      loadData(specialtyQualifier);
    });

    initSpinner();
    initRecyclerView();
  }

  private void initSpinner() {
    spinner = findViewById(R.id.spinner);
    ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.specialty, android.R.layout.simple_spinner_item);
    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner.setAdapter(spinnerAdapter);
    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        loadData(SpecialtyQualifier.values()[position]);
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {
        //NOP
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    mSwipeRefreshLayout.setRefreshing(false);
  }

  private void initRecyclerView() {
    mRecyclerView = findViewById(R.id.rv);
    mRecyclerView.setVerticalScrollBarEnabled(false);
    mRecyclerView.setHasFixedSize(true);
    LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
    mRecyclerView.setLayoutManager(mLayoutManager);
    mRecyclerView.setItemAnimator(null);
  }

  @Override
  public void updateList(List<PerformerViewModel> performers) {
    mRecyclerAdapter.updateAdapterList(performers);
    mSwipeRefreshLayout.setRefreshing(false);
  }

  @Override
  public void showLoadError(String msgError) {
    Snackbar snackbar = Snackbar.make(findViewById(R.id.root_main_activity_view), msgError, Snackbar.LENGTH_LONG);
    snackbar.setAction("Ok", new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        snackbar.dismiss();
      }
    }).show();
  }

  @Override
  public void openInfoActivity(int performerId) {
    Intent intent = new Intent(this, InfoActivity.class);
    intent.putExtra("performerId", performerId);
    startActivity(intent);
  }

  @Override
  public void initRecyclerAdapter(List<PerformerViewModel> performerList) {
    if (mRecyclerAdapter == null) {
      mRecyclerAdapter = new RecyclerAdapter(performerList, performer -> mMainActivityPresenter.onItemClick(performer));
      mRecyclerView.setAdapter(mRecyclerAdapter);
    }
  }

  private void loadData(SpecialtyQualifier specialtyQualifier) {
    switch (specialtyQualifier) {
      case ALL:
        mMainActivityPresenter.loadPerformerList();
        break;
      case MANAGER:
        mMainActivityPresenter.loadPerformerListByQualifier(SpecialtyQualifier.MANAGER);
        break;
      case DEVELOPER:
        mMainActivityPresenter.loadPerformerListByQualifier(SpecialtyQualifier.DEVELOPER);
        break;
    }
  }
}
