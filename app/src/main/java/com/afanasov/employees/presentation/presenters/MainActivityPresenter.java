package com.afanasov.employees.presentation.presenters;


import com.afanasov.employees.App;
import com.afanasov.employees.data.repository.IRepository;
import com.afanasov.employees.domain.SpecialtyQualifier;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.afanasov.employees.presentation.view.IMainActivityView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<IMainActivityView> {

  @Inject
  IRepository mRepository;
  private CompositeDisposable disposables = new CompositeDisposable();
  private List<PerformerViewModel> performerList = new ArrayList<>();

  public MainActivityPresenter() {
    App.getAppComponent().inject(this);
  }

  @Override
  public void onFirstViewAttach() {
    super.onFirstViewAttach();
    getViewState().initRecyclerAdapter(performerList);
  }

  public void loadPerformerList() {
    disposables.add(mRepository.loadAllPerformers()
      .subscribe(
        performerViewModels -> getViewState().updateList(performerViewModels),
        throwable -> getViewState().showLoadError("Internet connection error. Try again later..."))
    );
  }

  public void onItemClick(PerformerViewModel performer) {
    getViewState().openInfoActivity(performer.getPerformerId());
  }

  public void loadPerformerListByQualifier(SpecialtyQualifier specialtyQualifier) {
    disposables.add(mRepository.loadPerformerByQualifier(specialtyQualifier)
      .subscribe(
        performerViewModels -> getViewState().updateList(performerViewModels),
        throwable -> getViewState().showLoadError("Internet connection error. Try again later..."))
    );
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if(!disposables.isDisposed()){
      disposables.dispose();
    }
  }
}
