package com.afanasov.employees.presentation.view;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.afanasov.employees.R;
import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.afanasov.employees.presentation.model_view.SpecialtyViewModel;
import com.afanasov.employees.presentation.presenters.InfoActivityPresenter;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;


public class InfoActivity extends MvpAppCompatActivity implements IInfoActivityView {

  @InjectPresenter
  InfoActivityPresenter mInfoActivityPresenter;
  private ImageView avatar;
  private TextView firstName;
  private TextView lastName;
  private TextView age;
  private TextView birthdayPerformer;
  private TextView specialty;
  private int performerId;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowHomeEnabled(true);


    avatar = findViewById(R.id.avatar);
    firstName = findViewById(R.id.first_name);
    lastName = findViewById(R.id.last_name);
    birthdayPerformer = findViewById(R.id.birthday);
    age = findViewById(R.id.age);
    specialty = findViewById(R.id.specialty);

    Intent intent = getIntent();
    if (intent != null) {
      performerId = intent.getIntExtra("performerId", 0);
    }

  }

  @Override
  public void updatePerformer(PerformerViewModel performer) {
    firstName.setText(performer.getFirstName());
    lastName.setText(performer.getLastName());
    age.setText(String.format("%s %s", getString(R.string.age), performer.getAge()));
    birthdayPerformer.setText(performer.getBirthday());

    String allSpecialty = getAllSpecialty(performer.getSpecialty());
    specialty.setText(allSpecialty);

    if (performer.getAvatarUrl() == null || performer.getAvatarUrl().isEmpty()) {
      avatar.setImageResource(R.mipmap.ph);
    } else {
      Picasso.with(this)
        .load(performer.getAvatarUrl())
        .placeholder(R.mipmap.ph)
        .fit()
        .centerCrop()
        .error(R.mipmap.ph)
        .into(avatar);
    }
  }

  @Override
  public void showLoadError(String msgError) {
    Snackbar snackbar = Snackbar.make(findViewById(R.id.root_info_activity_view), msgError, Snackbar.LENGTH_LONG);
    snackbar.setAction("Ok", v -> snackbar.dismiss()).show();
  }

  @Override
  protected void onResume() {
    super.onResume();
    mInfoActivityPresenter.loadPerformer(performerId);
  }

  @Override
  public boolean onSupportNavigateUp() {
    onBackPressed();
    return true;
  }

  private String getAllSpecialty(List<SpecialtyViewModel> specialties) {
    List<String> specialtiesList = new ArrayList<>();
    for (SpecialtyViewModel item : specialties) {
      specialtiesList.add(item.getSpecialtyName());
    }
    return TextUtils.join(", ", specialtiesList);
  }
}
