package com.afanasov.employees.presentation.view;


import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface IInfoActivityView extends MvpView {

  void updatePerformer(PerformerViewModel performer);

  void showLoadError(String msgError);

}
