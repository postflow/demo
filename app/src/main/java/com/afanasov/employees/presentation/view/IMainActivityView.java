package com.afanasov.employees.presentation.view;


import com.afanasov.employees.presentation.model_view.PerformerViewModel;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

public interface IMainActivityView extends MvpView {
  void initRecyclerAdapter(List<PerformerViewModel> performerList);

  void updateList(List<PerformerViewModel> performerList);

  void showLoadError(String msgError);

  @StateStrategyType(SkipStrategy.class)
  void openInfoActivity(int performerId);


}
