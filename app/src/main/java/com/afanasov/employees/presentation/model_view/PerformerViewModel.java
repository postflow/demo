package com.afanasov.employees.presentation.model_view;


import java.util.List;

public class PerformerViewModel {

  private int performerId;
  private String firstName;
  private String lastName;
  private String birthday;
  private String avatarUrl;
  private String age;
  private List<SpecialtyViewModel> specialty ;


  public PerformerViewModel(int performerId, String firstName, String lastName, String birthday, String age, String avatarUrl, List<SpecialtyViewModel> specialtyList) {
    this.performerId = performerId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
    this.avatarUrl = avatarUrl;
    this.age = age;
    this.specialty = specialtyList;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getAge() {
    return age;
  }

  public String getLastName() {
    return lastName;
  }

  public String getBirthday() {
    return birthday;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public List<SpecialtyViewModel> getSpecialty() {
    return specialty;
  }

  public int getPerformerId() {
    return performerId;
  }








}
